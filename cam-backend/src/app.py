import json
import datetime
from urllib.parse import unquote

import boto3
from requests_toolbelt.multipart import decoder

BUCKET_NAME = "esp32-security-camera"


def fix_key_name(key):
    key = unquote(key)
    return key.replace("+", " ")


def create_signed_url():
    s3 = boto3.client("s3")

    now = datetime.datetime.now().isoformat()
    object_name = f"{now}.jpg"
    expiration = 300

    signed_url = s3.generate_presigned_url("put_object",
                                          {
                                              "Bucket": BUCKET_NAME,
                                              "Key": object_name,
                                              "ContentType": "application/jpg"
                                          },
                                          expiration)        

    return signed_url


def lambda_handler(event, context):
    body = event["body"]

    print("body: ")
    print(body)
    
    return {
        "statuscode": 200,
        "body": body
    }
