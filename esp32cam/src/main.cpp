#include <Arduino.h>
#include <base64.h>

#include <esp_task_wdt.h>
#include <esp_camera.h>

#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <HTTPClient.h>

#include <ArduinoJson.h>

#include "config.h"
#include "constants.h"

static unsigned long took_photo_t = 0;


static HTTPClient https_client;

void watchdog_setup() {
  esp_task_wdt_init(WATCHDOG_TIMEOUT, true);
  esp_task_wdt_add(NULL);
}

void camera_setup() {
  camera_config_t config;

  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;

  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;

  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;

  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;

  if (psramFound()) {
    Serial.println(F("PSRAM found"));

    config.frame_size = FRAMESIZE_UXGA;
    config.jpeg_quality = 63;
    config.fb_count = 1;
  } else {
    Serial.println(F("PSRAM not found"));

    config.frame_size = FRAMESIZE_SVGA;
    config.jpeg_quality = 63;
    config.fb_count = 1;
  }

  // Camera init
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.print(F("Camera init failed with error "));
    Serial.print(err);

    while (1);

    // ESP.restart();
  }
}

void wifi_setup() {
  float t = millis();

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    float dt = millis() - t;

    if (dt > 5 * 1000) {
      Serial.print(F("Trying to connect to "));
      Serial.print(WIFI_SSID);
      Serial.println(F(" ..."));

      t = millis();
    }
  }

  Serial.println(F("Connected to WiFi"));
  Serial.print(F("IP Address: "));
  Serial.println(WiFi.localIP());
}

void upload_photo(camera_fb_t *photo) {
  WiFiClientSecure wifi_client;
  wifi_client.setCACert(BACKEND_CERTIFICATE);

  Serial.print("Connecting to: ");
  Serial.print(BACKEND_URL);
  Serial.println(BACKEND_PATH);
  
  int connected = wifi_client.connect(BACKEND_URL, 443);

  if (!connected) {
    Serial.println("Could not connect to backend");
    return;
  }

  String filename = "esp32cam.jpg";
  String start_request = "";
  String end_request = "";
  
  start_request = start_request + "\n" + "--ESP32CAMBOUNDARY" + "\n" + "Content-Disposition: form-data; name=\"fileToUpload\";filename="+filename+"\n" + "Content-Type: file" + "\n" + "Content-Transfer-Encoding: binary" + "\n" + "\n";

  end_request = end_request + "\n" + "--ESP32CAMBOUNDARY--" + "\n";

  size_t len = photo->len + start_request.length() + end_request.length();
  
  wifi_client.print("POST ");
  wifi_client.print(BACKEND_PATH);
  wifi_client.println(" HTTP/1.1");

  wifi_client.print("Host: ");
  wifi_client.println(BACKEND_URL);

  wifi_client.println("Content-Type: multipart/form-data; boundary=ESP32CAMBOUNDARY");

  wifi_client.print("Content-Length: ");
  wifi_client.println(len);

  wifi_client.print(start_request);

  unsigned int sent_bytes = 0;

  uint8_t *ptr = photo->buf;
  const unsigned int PACKET_SIZE = 1024 * 10;

  while (1) {
    if ((sent_bytes + PACKET_SIZE) > photo->len) {
      size_t count = wifi_client.write(ptr, photo->len - sent_bytes);

      Serial.print("Sent: ");
      Serial.print(count);
      Serial.println(" bytes");

      break;
    }
    else {
      size_t count = wifi_client.write((uint8_t *)ptr, PACKET_SIZE);

      if (count != PACKET_SIZE) {
	Serial.println("Error while sending bytes");
	return;
      }

      Serial.print("Sent: ");
      Serial.print(count);
      Serial.println(" bytes");
      
      sent_bytes = sent_bytes + PACKET_SIZE;
      ptr = ptr + PACKET_SIZE;
    }
  }

  wifi_client.println(end_request);
}

void take_photo() {
  camera_fb_t * photo = NULL;

  photo = esp_camera_fb_get();  

  if(!photo) {
    Serial.println(F("Camera capture failed"));
    return;
  }

  Serial.print(F("Took photo, size: "));
  Serial.print(photo->len);
  Serial.println(F(" bytes"));

  upload_photo(photo);
  esp_camera_fb_return(photo);
}

void setup() {
  Serial.begin(9600);
  while (!Serial);

  watchdog_setup();
  
  wifi_setup();
  camera_setup();
}

void loop() {
  // esp_task_wdt_reset();

  unsigned long dt = millis() - took_photo_t;

  if (dt >= TAKE_PHOTO_INTERVAL) {
    float photo_start_t = millis();
    take_photo();
    float photo_end_t = millis();

    Serial.print(F("It took "));
    Serial.print((photo_end_t - photo_start_t) / 1000);
    Serial.println(F(" seconds to take the photo"));

    Serial.println();
    Serial.println();
    
    took_photo_t = millis();
  }
}
